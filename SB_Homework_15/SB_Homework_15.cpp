﻿#include <iostream>
#include "Math.h"
using namespace std;

const int LAST_EVEN_NUMBER = 10;

int main()
{
	for (int i = 0; i < LAST_EVEN_NUMBER; i += 2)
	{
		cout << i << "\n";
	}
	Numbers(LAST_EVEN_NUMBER, 1);
}